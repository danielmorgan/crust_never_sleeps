---
layout: base.njk
title: Crust Never Sleeps
templateEngineOverride: njk,md
---


<img src="img/crustneversleeps.jpg" width="284" height="400">


<br>
<img src="img/newtownlogo.jpg" alt="Newtown Logo" width="200" height="92"/>
 

### More info: [https://shithawkpunks.blogspot.com/p/crust-never-sleeps.html](https://shithawkpunks.blogspot.com/p/crust-never-sleeps.html)


Volume 49 w/ [Giant Peach](http://giantpeachsongs.bandcamp.com/) live set and interview (12/20/11) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps12-20-2011(GiantPeachLive).mp3)

Volume 48 w/ Male Nurses interview (12/13/11) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps12-13-2011.mp3)

Volume 47 w/ [Rational Animals](http://www.myspace.com/rationalanimals) interview (12/6/11) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps12-06-2011.mp3)

Volume 46 w/ Vaaska interview (11/29/11) [Stream of Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps11-29-2011.mp3)

Volume 45 w/ Omegas interview (11/22/11) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps11-22-2011.mp3)

Volume 44 w/ [Avon Ladies](http://cityslaang.com/al/) interview (11/15/11) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps11-15-2011.mp3)

Volume 43 (11/8/11) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps11-08-2011.mp3)

Volume 42 w/ Brain Slug live set and interview (11/1/11) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps11-1-11(BrainSlugLive).mp3)

Volume 41 w/ Milk Music interview (10/18/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps10-18-2011.mp3)

Volume 40 w/ Marked Men interview (9/27/2011) [Stream or Download](http://shithawkpunks.blogspot.com/2011/09/crust-never-sleeps-podcast-volume-40-ft.html)

Volume 39 w/ [Kira Roessler](http://www.hootpage.com/hoot_dos.html) interview (9/20/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps09-20-2011.mp3)

Volume 38 w/ [Night Birds](http://www.facebook.com/NGHTBRDS) live set and interview (9/13/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps09-13-2011(NightBirdsLive).mp3)

Volume 37 w/ [Iron Lung](http://lifeironlungdeath.blogspot.com/) interview (9/6/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeos09-06-2011.mp3)

Volume 36 w/ [Thee Oh Sees](http://www.theeohsees.com/) interview (8/23/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps08-23-2011.mp3)

Volume 35 (8/9/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps08-09-2011.mp3)

Volume 34 w/ [Coughing Fit](http://www.facebook.com/pages/Coughing-Fit/141290172599713) / [Drug Money](http://drugmoneyhc.blogspot.com/) live split set and interview (7/19/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps07-19-2011%20(CoughingFitandDrugMoneyLive).mp3)

Volume 33 w/ [Mike Watt](http://hootpage.com/) interview (7/12/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps07-12-2011.mp3)

Volume 32 w/ [Double Negative](http://www.facebook.com/thedoublenegative) interview (7/5/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps07-05-2011.mp3)

Volume 31 w/ [Man Without Plan](http://www.manwithoutplan.com/) live set and interview (6/28/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps06-28-2011.mp3)

Volume 30 w/ [Women in Prison](http://www.facebook.com/#%21/pages/WOMEN-IN-PRISON/349571203818) interview (6/21/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps06-21-2011.mp3)

Volume 29 w/ [Burning Love](http://burninglove416.blogspot.com/) interview (6/14/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps06-14-2011.mp3)

Volume 28 (5/24/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps05-24-2011.mp3)

Volume 27 w/ [Occult Detective Club](http://occultdetectiveclub.wordpress.com/) interview (5/17/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps05-17-2011.mp3)

Volume 26 (5/3/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps05-03-2011.mp3)

Volume 25 w/ [Hunx And His Punx](http://www.myspace.com/hunxsolo) interview (4/26/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps04-26-2011%28HunxAndHisPunxInterview%29.mp3)

Volume 24 (4/19/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps04-19-2011.mp3)

Volume 23 (4/12/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps04-12-2011.mp3)

Volume 22 (4/5/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps04-05-2011.mp3)

Volume 21 (3/29/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps03-29-2011.mp3)

Volume 20 w/ [Davila 666](http://www.myspace.com/davila666) interview (3/15/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps03-15-2011%28Davila666Interview%29.mp3)

Volume 19 w/ [Dipers](http://shithawkpunks.blogspot.com/p/dipers.html) live set and interview (2/22/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps02-22-2011%28DipersLive%29.mp3)

Volume 18 w/ [Death First](http://www.myspace.com/deathfirstbk) live set and interview (2/15/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps02-15-2011%28DeathFirstLive%29.mp3)

Volume 17 w/ [Gunfight!](http://www.myspace.com/gunfightband) live set and interview (2/8/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps02-08-2011%28Gunfight%21Live%29.mp3)

Volume 16 (2/1/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps02-01-2011.mp3)

Volume 15 (1/25/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps01-25-2011.mp3)

Volume 14 (1/18/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps01-18-2011.mp3)

Volume 13 w/ [Crazy Spirit](http://crazyspiritmotherfucker.blogspot.com/) live set and interview (1/11/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps01-11-2011%28CrazySpiritLive%29.mp3)

Volume 12 (1/4/2011) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps01-04-2011.mp3)

Volume 11 w/ Little Italy live set and interview (12/21/2010) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps12-21-2010%28LittleItalyLive%29.mp3)

Volume 10 (12/14/2010) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps12-14-2010.mp3)

Volume 9 (11/9/2010) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps11-09-2010.mp3)

Volume 8 w/ [Marvin Berry & The New Sound](http://www.myspace.com/marvinberryandthenewsound) live set and interview (11/2/2010) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps11-02-2010%28MarvinBerry&TheNewSoundlive%29.mp3)

Volume 7 w/ [The Men](http://wearethemen.blogspot.com/) live set and interview (10/26/2010) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps10-26-2010%28The%20Men%20live%29.mp3)

Volume 6 (10/19/2010) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps10-19-2010.mp3)

Volume 5 (10/12/2010) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps10-12-2010.mp3)

Volume 4 w/ [Red Dawn II](http://www.myspace.com/reddawnii) live set and interview (10/5/2010) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps10-05-2010%20%28Red%20Dawn%20II%20live%29.mp3)

Volume 3 (9/21/2010) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/CrustNeverSleeps09-21-2010.mp3)

Volume 2 (9/14/2010) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/Crust-Never-Sleeps-09-14-2010.mp3)

Volume 1 (9/7/2010) [Stream or Download](https://thestrange.foundation/static/crust_never_sleeps/Crust-Never-Sleeps-09-07-2010.mp3)
